import Slider from "react-slick";
import Style from "../../../App.module.css";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

const Carousel = () =>{
   const settings = {
    infinite : true,
    speed : 500,
    slidesToShow : 1,
    slidesToScroll : 1,
  
   };

    return(
        <>
          <div>
            <Slider {...settings}>
                <div className={Style.Slide1}>
                    <div className="row mt-4">
                      <div className="col-sm-6"></div>
                      <div className="col-sm-6" style={{paddingTop : "200px"}}>
                        <h1 className={Style.h1SLide1}>Welcome !</h1> 
                        <p>Autmn Collection Just Arrived  </p>
                        <button className="btn btn-primary">Check Our Collection</button>
                      </div>
                    </div>
                </div>
                <div className={Style.Slide2}>
                       <div className="row mt-4">
                          <div style={{marginTop : "200px", marginLeft : "50px"}} className="col-sm-4 text-white">
                              <p className={Style.pSlide2}>
                                Summer Collection 2023
                              </p>
                              <h1>Dress & Bags</h1>
                          </div>
                          <div className="col-sm-4 mt-4">
                            <div className={Style.buttonSlide2}>
                              <button style={{width : "200px" , height : "50px"}} className="btn btn-warning">SHOP NOW</button>
                            </div>
                            
                          </div>
                       </div>
                </div>
            </Slider>
          </div>
        </>
    )
}

export default Carousel