import Carousel from "../Carousel/Carousel"
import Style from "../../../App.module.css";
import LastestProduct from "../LastestProducts/LastestProducts";
import ViewAll from "../ViewAll/ViewAll";
const HomePageContent = () =>{
    return (
        <>
            <div className={Style.homepageContent}>
                <div>
                    <Carousel/>
                </div>
                <div className="container">
                    <LastestProduct/>
                    <ViewAll/>
                </div>
            </div>
        </>
    )
}

export default HomePageContent