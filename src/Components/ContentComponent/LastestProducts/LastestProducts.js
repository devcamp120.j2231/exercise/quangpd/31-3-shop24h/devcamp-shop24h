import Product1 from "../../../assets/ImagesProduct/Product1.jpeg";
import Product2 from "../../../assets/ImagesProduct/product2.jpg";
import Product3 from "../../../assets/ImagesProduct/product3.jpg";
import Product4 from "../../../assets/ImagesProduct/product4.jpg";
import Product5 from "../../../assets/ImagesProduct/product5.jpg";
import Product6 from "../../../assets/ImagesProduct/product6.jpg";
import Product7 from "../../../assets/ImagesProduct/product7.jpg";
import Product8 from "../../../assets/ImagesProduct/product8.jpg";
import Style from "../../../App.module.css";
const LastestProduct = () =>{
    return(
        <>
            <div className="mt-4">
                <div className="row text-center">
                    <div className="col-sm-12">
                        <h3>LASTEST PRODUCT</h3>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product1} alt="product1"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">OPPOSITE HEAVY TSHIRT</h5>
                                <p class="card-text">460,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product2} alt="product2"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">SIREN HEAVY TSHIRT</h5>
                                <p class="card-text">460,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product3} alt="product3"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">PARTY BUTTON TSHIRT</h5>
                                <p class="card-text">460,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product4} alt="product4"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">FICTION HEAVY TSHIRT</h5>
                                <p class="card-text">460,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                {/* .. */}
                <div className="row mt-3">
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product5} alt="product4"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">MULTI SNAPS DENIM WHITE JACKET</h5>
                                <p class="card-text">699,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product6} alt="product4"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">"G" BURGUNDY PUFFER JACKET</h5>
                                <p class="card-text">711,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product7} alt="product4"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">MULTI RIVETS PUFFER JACKET</h5>
                                <p class="card-text">1,711,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="card">
                            <img className="img-thumbnail" src={Product8} alt="product4"/>
                            <div class="card-body text-center">
                                <h5 class="card-title">MULTI SNAPS WASHED DENIM JACKET</h5>
                                <p class="card-text">1,211,000₫</p>
                                <a href="#">
                                    <button className={Style.btnProductBuy}>Shop now </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default LastestProduct