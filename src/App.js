import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Header from './Components/HeaderComponent/Header';
import Footer from './Components/FooterComponent/Footer';
import HomePageContent from './Components/ContentComponent/HomePageContent/HomePageContent';
import Slider from "react-slick";
function App() {
  return (
    <div >
      <Header/>
      <HomePageContent/>
      <Footer/>
    </div>
  );
}

export default App;
